import os
import json

from Crypto.PublicKey import RSA

import jwt


def private_key(keyfile):
    if os.path.exists(keyfile):
        with open(keyfile, "rb") as kf:
            key = RSA.importKey(kf.read())
    else:
        key = RSA.generate(2048)
        with open(keyfile, "wb") as kf:
            kf.write(key.exportKey())
    return key


agent_conf_dir = "/tmp/pyclsv/"


class Agent:
    def __init__(self, id, key=None, alg: jwt.JwaRsaCrypto=jwt.JwaRsaOaep(True), enc: jwt.Encryption=jwt.AesGcm(), conf_dir=None):
        self.conf_dir = os.path.join(conf_dir or agent_conf_dir, id)
        if not os.path.isdir(self.conf_dir):
            os.makedirs(self.conf_dir)

        if not isinstance(key, RSA.RsaKey):
            key = private_key(os.path.join(self.conf_dir, "privkey.pem"))

        self.jwe = jwt.JweCoder(alg, enc, key)

    def read_conf(self, filename, default=None):
        fn = os.path.join(self.conf_dir, filename)
        if not os.path.exists(fn):
            return default
        with open(fn, "r") as f:
            return json.load(f)

    def write_conf(self, filename, obj):
        with open(os.path.join(self.conf_dir, filename), "w") as f:
            return json.dump(obj, f)
