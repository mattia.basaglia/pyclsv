#!/usr/bin/env python3
import os
import uuid
import json
import socket
import threading

from Crypto.PublicKey import RSA

import jwt
import svmsg
import agent
import threadpool


class Connection:
    def __init__(self, server, connection, address, buffer_size, encoding="utf8"):
        self.server = server
        self.connection = connection
        self.address = address
        self.buffer_size = buffer_size
        self.encoding = encoding

    def __next__(self):
        data = self.connection.recv(self.buffer_size)
        data = self.decode(data)
        if not data:
            self.disconnect()
            raise StopIteration()
        return data

    def __iter__(self):
        return self

    def soft_disconnect(self):
        if self.connection:
            self.connection.shutdown(socket.SHUT_RD)
            self.connection.close()
            self.connection = None

    def disconnect(self):
        if self.connection:
            try:
                self.connection.shutdown(socket.SHUT_RDWR)
                self.connection.close()
            except OSError:
                pass
            self.connection = None

    def __enter__(self):
        return self

    def __exit__(self, *a, **k):
        self.disconnect()

    @property
    def connected(self):
        return self.connection

    def encode(self, data):
        if self.encoding and isinstance(data, str):
            return data.encode(self.encoding)
        return data

    def decode(self, data):
        if self.encoding:
            try:
                return data.decode(self.encoding)
            except UnicodeDecodeError:
                return ""
        return data

    def write(self, data):
        return self.connection.send(self.encode(data))

    def writeline(self, data):
        data = self.encode(data)
        return self.connection.send(data+b"\r\n")


class ServerSocket:
    def __init__(self, port, address='127.0.0.1', buffer_size=1024):
        self.bind_address = (address, port)
        self.buffer_size = buffer_size
        self.socket = None
        self.mutex = threading.Lock()

    @property
    def connected(self):
        with self.mutex:
            return self.socket

    def connect(self):
        with self.mutex:
            self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            self.socket.bind(self.bind_address)
            self.socket.listen(1)

    def disconnect(self):
        with self.mutex:
            if self.socket:
                self.socket.shutdown(socket.SHUT_RDWR)
                self.socket.close()
                self.socket = None

    def __enter__(self):
        self.connect()
        return self

    def __exit__(self, *a, **k):
        self.disconnect()

    def accept(self):
        with self.mutex:
            socket = self.socket
        conn, address = socket.accept()
        return Connection(self, conn, address, self.buffer_size)

    @property
    def address(self):
        return "%s:%s" % self.bind_address


class Client:
    def __init__(self, connected, key, name, channels):
        self.mutex = threading.Lock()
        self.connected = list(connected)
        self.key = key
        self._name = name or "Anon?"
        self.channels = set(channels)

    @property
    def key(self):
        return self._key

    @key.setter
    def key(self, key):
        with self.mutex:
            self._key = key
            if key:
                self.hashkey = key.export_key('DER')

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        with self.mutex:
            self._name = name

    def add_connected(self, connected: "ConnectedClient"):
        with self.mutex:
            self.connected.append(connected)

    def rm_connected(self, connected: "ConnectedClient"):
        with self.mutex:
            idx = self.connected.index(connected)
            self.connected.pop(idx)

    def add_channel(self, channel):
        with self.mutex:
            if channel == self._name:
                return False
            if channel not in self.channels:
                self.channels.add(channel)
                return True
            return False

    def rm_channel(self, channel):
        with self.mutex:
            if channel in self.channels:
                self.channels.remove(channel)
                return True
            return False

    def in_channel(self, channel):
        with self.mutex:
            return channel in self.channels or channel == self._name

    def to_data(self):
        return {
            "key": self.key.exportKey().decode("utf8"),
            "name": self.name,
            "channels": list(self.channels),
        }

    @classmethod
    def from_data(cls, data):
        return cls(
            [],
            RSA.importKey(data["key"]),
            data["name"],
            data["channels"]
        )


class ConnectedClient:
    def __init__(self, client: Client, connection: Connection):
        self.client = client
        self.connection = connection
        self.refresh_challenge()

    def refresh_challenge(self):
        self.challenge = uuid.uuid4()
        return self.challenge

    @property
    def name(self):
        if self.client:
            return self.client.name
        return "%s:%s" % self.connection.address

    def log_event(self, msg):
        print("%s (%s:%s) %s" % (
            self.name,
            self.connection.address[0],
            self.connection.address[1],
            msg
        ))


class ClientThread(threadpool.Thread):
    def __init__(self, controller, client: ConnectedClient):
        super().__init__()
        self.controller = controller
        self.client = client

    def run(self):
        with self.client.connection:
            self.controller.client_connected(self.client)
            for line in self.client.connection:
                self.controller.client_input(self.client, line.rstrip("\r\n"))
            self.controller.client_input_end(self.client)

    def stop(self):
        self.controller.client_disconnecting(self.client)
        self.client.connection.disconnect()

    @property
    def should_run(self):
        return self.client.connection.connection


class ServerController(agent.Agent):
    def __init__(self, server, threads, cmds, id, *a, **kw):
        super().__init__(id, *a, **kw)
        self.server = server
        self.threads = threads
        self.mutex = threading.Lock()
        self.clients = {}
        self.pubkey = self.jwe.key.publickey().exportKey()
        self.cmds = cmds

    def run(self):
        self.load_persistent()
        with self.server as server:
            with self.threads as threads:
                threads.start_thread(threadpool.GCThread(threads))
                try:
                    print("Listening on %s" % server.address)
                    while True:
                        try:
                            connection = server.accept()
                            try:
                                threads.start_thread(ClientThread(self, ConnectedClient(None, connection)))
                                print("%s active connections" % (len(threads) - 1))
                            except threads.TooMany:
                                connection.writeline("Sorry, server is busy")
                                connection.soft_disconnect()
                        except Exception as e:
                            print(e)
                except KeyboardInterrupt:
                    pass
                finally:
                    server.disconnect()
                    self.save_persistent()
        print("Server Stopped")

    def _svmsg(self, type, msg):
        return svmsg.SvMessage(type, None, None, msg)

    def client_connected(self, client: ConnectedClient):
        client.log_event("joined")
        with self.mutex:
            self.client_send_unencrypted(client, svmsg.SvMessage(
                            "key", None, client.name, self.pubkey
            ))

    def client_unencrypted(self, client: ConnectedClient, line):
        try:
            data = json.loads(line)
            msg = svmsg.SvMessage.from_data(data)
        except (ValueError, KeyError):
            self.client_send_unencrypted(client, self._svmsg("error", "Invalid message"))
            return

        if msg.type == "key":
            with self.mutex:
                if not client.client:
                    try:
                        key = RSA.importKey(msg.msg)
                        hashkey = key.export_key('DER')

                        if hashkey in self.clients:
                            reg_client = self.clients[hashkey]
                            reg_client.add_connected(client)
                        else:
                            reg_client = Client([client], key, client.name, [])
                            self.clients[hashkey] = reg_client
                        client.client = reg_client
                        client.log_event("sent public key")

                        self.client_send(client, svmsg.SvMessage(
                            "nick", None, client.name, "Welcome %s" % client.name
                        ))
                        return
                    except ValueError:
                        pass
            client.log_event("has sent an invalid key")
            self.client_send_unencrypted(client, self._svmsg("error", "Invalid key"))
            return

        self.client_send_unencrypted(client, self._svmsg("error", "Encryption required"))

    def client_send(self, client: ConnectedClient, msg: svmsg.SvMessage):
        msg.challenge = str(client.refresh_challenge())
        data = self.jwe.encode_json(client.client.key, msg.to_data())
        client.connection.writeline(data)

    def client_send_all(self, client, msg: svmsg.SvMessage):
        if isinstance(client, ConnectedClient):
            client = client.client
        for conn in client.connected:
            self.client_send(conn, msg)

    def client_send_unencrypted(self, client: ConnectedClient, msg: svmsg.SvMessage):
        msg.challenge = str(client.refresh_challenge())
        client.connection.writeline(json.dumps(msg.to_data()))

    def client_input(self, client: ConnectedClient, line):
        try:
            if "{" in line:
                return self.client_unencrypted(client, line)
            deco = self.jwe.decode_json(line)
            msg = svmsg.SvMessage.from_data(deco)
        except (ValueError, KeyError, jwt.DecodeError):
            self.client_send(client, self._svmsg("error", "Invalid message"))
            return

        if msg.challenge != str(client.challenge):
            self.client_send(client, self._svmsg("error", "Invalid challenge"))
            return

        msg.msgfrom = client.name
        msg.msg = msg.msg.lstrip("\r\n") if msg.msg else ""

        print(msg.format())
        if msg.type in self.cmds:
            self.cmds[msg.type](self, client, msg)
        else:
            self.client_send(client, self._svmsg("error", "Unknown message"))

    def client_input_end(self, client: ConnectedClient):
        if client.client:
            client.client.rm_connected(client)
        client.log_event("left")

    def client_disconnecting(self, client: ConnectedClient):
        pass

    def clients_in(self, channel):
        cls = []
        with self.mutex:
            for cl in self.clients.values():
                if cl.in_channel(channel):
                    cls.append(cl)
        return cls

    def broadcast(self, channel, msg: svmsg.SvMessage):
        for cl in self.clients_in(channel):
            self.client_send_all(cl, msg)

    def msg_dest(self, name):
        if not isinstance(name, str) or len(name) < 2:
            return MsgDest(self)

        if name.startswith("#"):
            return MsgDestChannel(self,name.lower())

        if name.startswith("@"):
            lname = name.lower()[1:]
            with self.mutex:
                for client in self.clients.values():
                    if client.name.lower() == lname:
                        return MsgDestClient(self,client)

        return MsgDest(self)

    def save_persistent(self):
        with self.mutex:
            self.write_conf("sv.json", [
                cl.to_data()
                for cl in self.clients.values()
            ])

    def load_persistent(self):
        with self.mutex:
            self.clients = {}
            for obj in self.read_conf("sv.json", []):
                cl = Client.from_data(obj)
                self.clients[cl.hashkey] = cl


def cmd_rename(server: ServerController, client: ConnectedClient, msg: svmsg.SvMessage):
    name = client.client.name
    if msg.msg and msg.msg.isalnum():
        with server.mutex:
            for cli in server.clients.values():
                if cli.name == msg.msg and cli is not client:
                    break
            else:
                client.client.name = name = msg.msg

    server.client_send_all(client, svmsg.SvMessage(
        "nick", None, name, "Renamed to %s" % name
    ))


class MsgDest:
    def __init__(self, server: ServerController):
        self.server = server

    def __str__(self):
        raise NotImplementedError()

    def send(self, msg: svmsg.SvMessage):
        raise NotImplementedError()

    def is_valid(self):
        return False

    def part(self, client: Client):
        raise NotImplementedError()

    def join(self, client: Client):
        raise NotImplementedError()


class MsgDestChannel(MsgDest):
    def __init__(self, server: ServerController, name):
        super().__init__(server)
        self.name = name

    def __str__(self):
        return self.name

    def send(self, msg: svmsg.SvMessage):
        msg.to = self.name
        self.server.broadcast(self.name, msg)

    def is_valid(self):
        return True

    def join(self, client: Client):
        return client.add_channel(self.name)

    def part(self, client: Client):
        return client.rm_channel(self.name)


class MsgDestClient(MsgDest):
    def __init__(self, server: ServerController, client: Client):
        super().__init__(server)
        self.client = client

    @property
    def name(self):
        return self.client.name

    def __str__(self):
        return self.client.name

    def send(self, msg: svmsg.SvMessage):
        msg.to = self.client.name
        self.server.client_send_all(self.client, msg)

    def is_valid(self):
        return True

    def join(self, client: Client):
        self.client.add_channel(client.name)
        return client.add_channel(self.client.name)

    def part(self, client: Client):
        self.client.rm_channel(client.name)
        return client.rm_channel(self.client.name)


def cmd_join(server: ServerController, client: ConnectedClient, msg: svmsg.SvMessage):
    dest = server.msg_dest(msg.msg)
    if dest.is_valid():
        added = dest.join(client.client)
        if added:
            dest.send(svmsg.SvMessage(
                "join", client.name, dest.name, "joined %s" % dest
            ))
        else:
            server.client_send(client, svmsg.SvMessage(
                "error", None, client.name, "You already are in %s" % dest
            ))
    else:
        server.client_send(client, svmsg.SvMessage(
            "error", None, client.name, "Invalid chat name"
        ))


def cmd_part(server: ServerController, client: ConnectedClient, msg: svmsg.SvMessage):
    dest = server.msg_dest(msg.msg)
    if dest:
        added = dest.part(client.client)
        if added:
            partmsg = svmsg.SvMessage(
                "part", client.name, dest.name, "left %s" % dest
            )
            dest.send(partmsg)
            server.client_send_all(client, partmsg)
        else:
            server.client_send(client, svmsg.SvMessage(
                "error", None, client.name, "You aren't in %s" % dest
            ))
    else:
        server.client_send(client, svmsg.SvMessage(
            "error", None, client.name, "Invalid channel name"
        ))


def cmd_msg(server: ServerController, client: ConnectedClient, msg: svmsg.SvMessage):
    dest = server.msg_dest(msg.to)
    if not dest.is_valid():
        server.client_send(client, svmsg.SvMessage(
            "error", None, client.name, "Not a valid recipient"
        ))
    elif client.client.in_channel(dest.name):
        dest.send(msg)
    else:
        server.client_send(client, svmsg.SvMessage(
            "error", None, client.name, "You are not in %s" % dest
        ))


def cmd_users(server: ServerController, client: ConnectedClient, msg: svmsg.SvMessage):
    dest = server.msg_dest(msg.msg)
    if not dest.is_valid():
        server.client_send(client, svmsg.SvMessage(
            "error", None, client.name, "Not a valid recipient"
        ))
    elif client.client.in_channel(dest.name):
        with server.mutex:
            names = [
                cl.name
                for cl in server.clients.values()
                if cl.in_channel(dest.name)
            ]
        server.client_send(client, svmsg.SvMessage(
            "users", None, client.name, ", ".join(names)
        ))
    else:
        server.client_send(client, svmsg.SvMessage(
            "error", None, client.name, "You are not in %s" % dest
        ))


ServerController(
    ServerSocket(5002),
    threadpool.Threads(3),
    {
        "nick": cmd_rename,
        "quit": lambda *a: None,
        "ping": lambda server, client, msg:
            server.client_send_all(client, svmsg.SvMessage(
                "pong", None, client.name, msg.msg
            )),
        "msg": cmd_msg,
        "join": cmd_join,
        "part": cmd_part,
        "me": cmd_msg,
        "channels": lambda server, client, msg:
            server.client_send(client, svmsg.SvMessage(
                "channels", None, client.name, ", ".join(client.client.channels)
            )),
        "users": cmd_users,
    },
    "server"
).run()
