import threading


class Threads:
    class TooMany(Exception):
        pass

    def __init__(self, max_threads=None):
        self.threads = []
        self.mutex = threading.Lock()
        self.max_threads = max_threads

    def __enter__(self):
        return self

    def __exit__(self, *a, **k):
        self.stop_all()

    def __next__(self):
        thread = self.fetch_thread()
        self.start_thread(thread)
        return thread

    def __iter__(self):
        return self

    def start_thread(self, thread):
        with self.mutex:
            threads = self.threads
            self.threads = []
            for old_thread in threads:
                if old_thread.isAlive():
                    self.threads.append(old_thread)

            if self.max_threads and len(self.threads) >= self.max_threads:
                raise self.TooMany()

            self.threads.append(thread)
            thread.start()

    def start(self, *a, **k):
        self.start_thread(threading.Thread(*a, **k))

    def stop(self, thread):
        with self.mutex:
            if thread not in self.threads:
                raise IndexError("Not an owned thread")
            self._on_stop(thread)
            index = self.threads.index(thread)
            self.threads.pop(index)

    def _on_stop(self, thread):
        try:
            thread.stop()
        except AttributeError:
            pass
            thread.join()

    def stop_all(self):
        with self.mutex:
            threads = self.threads
            self.threads = []

        for thread in threads:
            self._on_stop(thread)

    def __len__(self):
        return len(self.threads)

    def garbage_collect(self):
        with self.mutex:
            threads = []
            stopped = 0
            for thread in self.threads:
                if not isinstance(thread, Thread) or thread.should_run:
                    threads.append(thread)
                else:
                    self._on_stop(thread)
                    stopped += 1
            self.threads = threads
        return stopped


class Thread(threading.Thread):
    def log(self, message):
        print("[%s] %s" % (self.ident, message))

    @property
    def should_run(self):
        return True

    def stop(self):
        pass


class GCThread(Thread):
    def __init__(self, threads):
        super().__init__()
        self.threads = threads
        self.mutex = threading.Lock()
        self.cond = threading.Condition(self.mutex)

    def run(self):
        while True:
            with self.mutex:
                if not self.threads:
                    return
                collected = self.threads.garbage_collect()
                if collected:
                    self.log("Collected %s" % collected)
                self.cond.wait(5)

    def stop(self):
        with self.mutex:
            self.threads = None
            self.cond.notify()

    @property
    def should_run(self):
        return self.threads
