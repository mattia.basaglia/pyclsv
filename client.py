#!/usr/bin/env python3
import os
import sys
import json
import socket
import select
import argparse
import readline
import threading

from Crypto.PublicKey import RSA

import jwt
import svmsg
import agent
import threadpool


class ClientSocket:
    def __init__(self, port, address='127.0.0.1', buffer_size=1024, encoding="utf8"):
        self.connect_address = (address, port)
        self.buffer_size = buffer_size
        self.socket = None
        self.mutex = threading.Lock()
        self.encoding = encoding

    @property
    def connected(self):
        with self.mutex:
            return self.socket

    def connect(self):
        with self.mutex:
            self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.socket.connect(self.connect_address)

    def encode(self, data):
        if self.encoding and isinstance(data, str):
            return data.encode(self.encoding)
        return data

    def decode(self, data):
        if self.encoding:
            try:
                return data.decode(self.encoding)
            except UnicodeDecodeError:
                return ""
        return data

    def _next_block(self):
        while self.connected:
            with self.mutex:
                if not self.socket:
                    raise StopIteration()
                socket = self.socket
            rlist, _, xlist = select.select([socket], [], [socket], 1)
            if rlist or xlist:
                break

    def __iter__(self):
        return self

    def __next__(self):
        self._next_block()
        with self.mutex:
            if not self.socket:
                raise StopIteration()
            data = self.socket.recv(self.buffer_size)
        data = self.decode(data)

        if not data:
            self.disconnect()
            raise StopIteration()
        return data

    def disconnect(self):
        with self.mutex:
            if self.socket:
                self.socket.shutdown(socket.SHUT_RDWR)
                self.socket.close()
                self.socket = None

    def send(self, message):
        with self.mutex:
            self.socket.send(self.encode(message))


class NetIn(threadpool.Thread):
    def __init__(self, client_controller):
        super().__init__()
        self.client_controller = client_controller
        self.mutex = threading.Lock()
        self._should_run = True

    def run(self):
        with self.mutex:
            if not self._should_run:
                return
        old = ""
        for data in self.client_controller.client_socket:
            data = data.replace("\r", "")
            if "\n" not in data:
                old += data
            else:
                lines = (old+data).split("\n")
                old = lines.pop(-1)
                for line in lines:
                    self.client_controller.net_input(line)
            with self.mutex:
                if not self._should_run:
                    return

    def stop(self):
        with self.mutex:
            print("stopping net")
            self._should_run = False

    @property
    def should_run(self):
        return self._should_run and self.client_controller.client_socket.connected


class StdIn(threadpool.Thread):
    def __init__(self, client_controller, stream=input):
        super().__init__()
        self.client_controller = client_controller
        self.stream = stream
        self.mutex = threading.Lock()

    def run(self):
        readline.parse_and_bind("tab: complete")

        while True:
            with self.mutex:
                if not self.stream:
                    return
                try:
                    line = self.stream().rstrip("\r\n")
                except (KeyboardInterrupt, EOFError, StopIteration):
                    return

            self.client_controller.stdin_input(line)
            if not self.client_controller.client_socket.connected:
                self.stop()

    def stop(self):
        with self.mutex:
            print("stopping stdin")
            self.stream = None

    @property
    def should_run(self):
        return self.stream


class ClientController(agent.Agent):
    def __init__(self, client_socket, id, *a, **kw):
        super().__init__(id, *a, **kw)
        self.client_socket = client_socket
        self.challenge = ""
        self.name = "Anon"
        self.quit = False
        self.svkey = None
        self.mutex = threading.Lock()
        self.active_channel = None

    def run(self):
        self.client_socket.connect()
        self.thread_netin = NetIn(self)
        self.thread_netin.start()
        self.send_unencrypted(svmsg.SvMessage("key", None, None, self.jwe.key.publickey().exportKey()))
        self.thread_stdin = StdIn(self)
        self.thread_stdin.run()
        if self.svkey and not self.quit:
            self.send(svmsg.SvMessage("quit", self.name, None, "", self.challenge))
        self.client_socket.disconnect()
        self.thread_netin.stop()
        self.thread_netin.join()

    def send_unencrypted(self, msg):
        if not self.client_socket.connected:
            return
        self.client_socket.send(json.dumps(msg.to_data()))

    def send(self, msg):
        if not self.client_socket.connected:
            return
        if not self.svkey:
            print("Trying to send message without server key")
            return
        token = self.jwe.encode_json(self.svkey, msg.to_data())
        self.client_socket.send(token)

    def stdin_input(self, line):
        if not line:
            return

        msgfrom = self.name

        if line[0] == "/":
            type, msgtxt = line[1:].split(None, 1) if " " in line else (line[1:], "")
            msgto = None
        else:
            type = "msg"
            msgtxt = line
            msgto = self.active_channel

        if type == "quit" or type == "q":
            self.quit = True
            self.thread_stdin.stop()
        elif type == "use":
            self.active_channel = msgtxt
            return
        elif type == "join":
            self.active_channel = msgtxt
        elif type in {"me", "msg"}:
            msgto = self.active_channel
        elif type in {"part", "users"} and not msgtxt:
            msgtxt = self.active_channel

        self.send(svmsg.SvMessage(type, msgfrom, msgto, msgtxt, self.challenge))

    def net_input(self, line):
        if "{" in line:
            return self.net_input_unencrypted(line)
        try:
            deco = self.jwe.decode_json(line)
            msg = svmsg.SvMessage.from_data(deco)
        except (ValueError, KeyError, jwt.DecodeError):
            print("Invalid message")
            return

        self.challenge = msg.challenge

        if msg.type == "ping":
            self.send(svmsg.SvMessage("pong", self.name, None, msg.msg, self.challenge))
            return

        if msg.type == "nick":
            self.name = msg.to
        print(msg.format())

    def net_input_unencrypted(self, line):
        try:
            data = json.loads(line)
            msg = svmsg.SvMessage.from_data(data)
        except (ValueError, KeyError):
            return

        if msg.type == "key":
            with self.mutex:
                if not self.svkey:
                    try:
                        self.svkey = RSA.importKey(msg.msg)
                        self.name = msg.to
                        return
                    except ValueError:
                        pass


parser = argparse.ArgumentParser()
parser.add_argument(
    "--id", "-i",
    default="client"
)
ns = parser.parse_args()

ClientController(
    ClientSocket(5002),
    ns.id,
).run()
