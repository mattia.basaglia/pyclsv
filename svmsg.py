class SvMessage:
    def __init__(self, type, msgfrom, to, msg, challenge=None):
        self.type = type
        self.msgfrom = msgfrom
        self.to = to
        if isinstance(msg, bytes):
            msg = msg.decode('utf8')
        self.msg = msg
        self.challenge = challenge

    def to_data(self):
        return {
            "type": self.type,
            "from": self.msgfrom,
            "to": self.to,
            "msg": self.msg,
            "challenge": self.challenge,
        }

    @classmethod
    def from_data(cls, data):
        return cls(data["type"], data["from"], data["to"], data["msg"], data["challenge"])

    def format(self):
        if self.msgfrom is None:
            f = "!"
            prefix = ""
        else:
            f = self.msgfrom
            prefix = self.to + " " if self.to else ""

        if self.type == "msg" or (f == "!" and self.type in ("nick")):
            return "%s<%s> %s" % (prefix, f, self.msg)
        else:
            txt = self.msg or self.type
            if txt:
                txt = " " + txt
            return "%s*%s%s*" % (prefix, f, txt)
