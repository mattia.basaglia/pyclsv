import hmac
import json
import time
import base64
import hashlib
import datetime

from Crypto.Cipher import PKCS1_OAEP, PKCS1_v1_5, AES
from Crypto.Hash import SHA, SHA256
from Crypto.Util import number as c_num
from Crypto.Random import get_random_bytes


# https://tools.ietf.org/html/rfc7518 JSON Web Algorithms (JWA)
class JsonWebAlgorithm:
    def __init__(self, name):
        self.name = name

    def digest(self, key, payload):
        raise NotImplementedError()

    def check_digest(self, key, payload, digest):
        raise NotImplementedError()


class JwaHmac(JsonWebAlgorithm):
    def __init__(self, bits):
        super().__init__("HS%s" % bits)
        self.hash = getattr(hashlib, "sha%s" % bits)

    def digest(self, key, payload):
        return base64.urlsafe_b64encode(
            hmac.new(key.encode("utf-8"), payload, self.hash).digest()
        )

    def check_digest(self, key, payload, digest):
        return self.digest(key, payload) == digest


class JwaRsaCrypto(JsonWebAlgorithm):
    def cipher(self, key):
        raise NotImplementedError()

    def plaintext_chunk_size(self, key):
        raise NotImplementedError()

    def ciphertext_chunk_size(self, key):
        return c_num.ceil_div(c_num.size(key.n), 8)

    def encrypt(self, key, payload):
        cipher = self.cipher(key)
        enc = b''
        chunk_len = self.plaintext_chunk_size(key)
        for i in range(0, len(payload), chunk_len):
            chunk = payload[i:i+chunk_len]
            enc += cipher.encrypt(chunk)
        return enc

    def decrypt(self, key, enc):
        cipher = self.cipher(key)
        decrypted = b''
        chunk_len = self.ciphertext_chunk_size(key)
        for i in range(0, len(enc), chunk_len):
            chunk = enc[i:i+chunk_len]
            decrypted += cipher.decrypt(chunk)
        return decrypted

    def digest(self, key, payload):
        return base64.urlsafe_b64encode(self.encrypt(key, payload))

    def check_digest(self, key, payload, digest):
        return self.decrypt(key, digest) == payload


class JwaRsaOaep(JwaRsaCrypto):
    def __init__(self, use_sha256):
        super().__init__("RSA-OAEP%s" % ("-256" if use_sha256 else ""))
        self.algo = SHA256 if use_sha256 else SHA

    def cipher(self, key):
        return PKCS1_OAEP.new(key)

    def plaintext_chunk_size(self, key):
        k = c_num.ceil_div(c_num.size(key.n), 8)
        return k - 2*self.algo.digest_size -2


class JwaRsaPkcs1(JwaRsaCrypto):
    def __init__(self):
        super().__init__("RSA1_5")

    def cipher(self, key):
        return PKCS1_v1_5.new(key)

    def plaintext_chunk_size(self, key):
        k = c_num.ceil_div(c_num.size(key.n), 8)
        return k - 11


class JwaRsaPkcs1(JsonWebAlgorithm):
    cipher = PKCS1_OAEP

    def __init__(self):
        super().__init__("RSA1_5")


class JwaNone(JsonWebAlgorithm):
    def __init__(self):
        super().__init__("none")

    def digest(self, key, payload):
        return b""

    def check_digest(self, key, payload, digest):
        return digest == b""


class JwaFactory:
    def get_algorithm(self, name):
        if isinstance(name, JsonWebAlgorithm):
            return name
        if name[:2] == "HS":
            bits = name[2:]
            if bits in {"256", "384", "512"}:
                return JwaHmac(bits)
        if name == "RSA1_5":
            return JwaRsaPkcs1()
        if name.startswith("RSA-OAEP"):
            return JwaRsaOaep(name.endswith("-256"))
        if name == "none":
            return JwaNone()
        if name == "A256GCM":
            return AesGcm()
        raise KeyError(name)


class DecodeError(Exception):
    pass


def json_base64_encode(data):
    return base64.urlsafe_b64encode(json.dumps(data).encode("utf-8"))


def json_base64_decode(data):
    try:
        return json.loads(base64.urlsafe_b64decode(data+b"==").decode("utf-8"))
    except json.decoder.JSONDecodeError:
        raise DecodeError("Invalid JSON")
    except UnicodeDecodeError:
        raise DecodeError("Invalid UTF-8")


# https://jwt.io/introduction/
# https://tools.ietf.org/html/rfc7519 JSON Web Token (JWT)
class JsonWebToken:
    def __init__(self, algorithm, key, payload):
        self.algorithm = algorithm
        self.payload = payload
        self.key = key

    def encode(self):
        header = json_base64_encode({
            "alg": self.algorithm.name,
            "typ": "JWT"
        })
        payload = json_base64_encode(self.payload)
        digest_payload = b"%s.%s" % (header, payload)
        digest = self.algorithm.digest(self.key, digest_payload)
        return b"%s.%s" % (digest_payload, digest)


class JwtDecoder:
    def __init__(self, algorithms, data):
        self.algorithms = algorithms
        self.received = time.time()

        if isinstance(data, str):
            data = data.encode("utf-8")

        try:
            eheader, epayload, self.digest = data.split(b'.')
        except ValueError:
            raise DecodeError("Invalid token format")

        self.digest_payload = data.rsplit(b".", 1)[0]
        self.header = json_base64_decode(eheader)

        algoname = self.header.get("alg", "none")
        for algorithm in self.algorithms:
            if isinstance(algorithm, str) and algoname == algoname:
                break
            elif isinstance(algorithm, JsonWebAlgorithm) and algoname == algorithm.name:
                break
        else:
            raise DecodeError("Invalid signature algorithm")
        if "typ" in self.header and self.header["typ"].upper() != "JWT":
            raise DecodeError("Invalid token type")
        self.algorithm = JwaFactory().get_algorithm(algoname)

        self.payload = json_base64_decode(epayload)

    def validate(self, key):
        if not self.algorithm.check_digest(key, self.digest_payload, self.digest):
            raise DecodeError("Token digest not matching")

        try:
            if "exp" in self.payload and self.payload["exp"] < self.received:
                raise DecodeError("Token expired")
            if "nbf" in self.payload and self.payload["nbf"] > self.received:
                raise DecodeError("Token not yet valid")
        except TypeError:
            raise DecodeError("Invalid NumericDate")

        return JsonWebToken(self.algorithm, key, self.payload)


class Encryption:
    name = ""

    def encrypt(self, key, payload, iv, aad):
        raise NotImplementedError()


class AesGcm(Encryption):
    name = "A256GCM"

    def __init__(self):
        pass

    def encrypt(self, key, payload, iv, aad):
        cipher = AES.new(key[:32], AES.MODE_GCM, nonce=iv)
        cipher.update(aad)
        ciphertext, tag = cipher.encrypt_and_digest(payload)
        return ciphertext, tag

    def decrypt(self, key, ciphertext, tag, iv, aad):
        """
        Raises ValueError, KeyError
        """
        cipher = AES.new(key[:32], AES.MODE_GCM, nonce=iv)
        cipher.update(aad)
        return cipher.decrypt_and_verify(ciphertext, tag)


class Jwe:
    def __init__(
        self,
        alg: JwaRsaCrypto,
        enc: Encryption,
        recipient_pubkey,
        cek=None,
        iv=None,
        cek_len=256,
        iv_len=96
    ):
        self.algorithm = alg
        self.encryption = enc
        self.recipient_pubkey = recipient_pubkey
        self.content_encryption_key = cek or get_random_bytes(cek_len//8)
        self.initialization_vector = iv or get_random_bytes(iv_len//8)

    def encode(self, plaintext):
        protected_header = json_base64_encode({
            "alg": self.algorithm.name,
            "enc": self.encryption.name,
            "typ": "JOSE",
        })
        enc_key = self.algorithm.encrypt(self.recipient_pubkey, self.content_encryption_key)
        aad = protected_header
        ciphertext, tag = self.encryption.encrypt(enc_key, plaintext, self.initialization_vector, aad)
        return b".".join((
            protected_header,
            base64.urlsafe_b64encode(enc_key),
            base64.urlsafe_b64encode(self.initialization_vector),
            base64.urlsafe_b64encode(ciphertext),
            base64.urlsafe_b64encode(tag)
        ))

    @classmethod
    def decode(cls, buffer, privkey):
        protected_header, enc_key, iv, ciphertext, tag = buffer.split(b".")
        header = json_base64_decode(protected_header)
        alg = JwaFactory().get_algorithm(header["alg"])
        enc = JwaFactory().get_algorithm(header["enc"])
        enc_key = base64.urlsafe_b64decode(enc_key)
        cek = alg.decrypt(privkey, enc_key)
        iv = base64.urlsafe_b64decode(iv)
        ciphertext = base64.urlsafe_b64decode(ciphertext)
        tag = base64.urlsafe_b64decode(tag)
        jwe = cls(alg, enc, privkey, cek, iv)
        aad = protected_header
        plaintext = enc.decrypt(enc_key, ciphertext, tag, iv, aad)
        return jwe, plaintext


class JweCoder:
    def __init__(self, alg: JwaRsaCrypto, enc: Encryption, privkey,
                 allowed_algs=None, allowed_enc=None):
        self.alg = alg
        self.enc = enc
        self.key = privkey
        self.allowed_algs = allowed_algs or [alg.name]
        self.allowed_enc = allowed_enc or [enc.name]

    def encode(self, recipient_pubkey, plaintext):
        return Jwe(self.alg, self.enc, recipient_pubkey).encode(plaintext)

    def encode_json(self, recipient_pubkey, data):
        return self.encode(recipient_pubkey, json.dumps(data).encode("utf8"))

    def decode(self, buffer):
        jwe, plainext = Jwe.decode(buffer, self.key)
        if jwe.algorithm.name not in self.allowed_algs:
            return None
        if jwe.encryption.name not in self.allowed_enc:
            return None
        return plainext

    def decode_json(self, buffer):
        if isinstance(buffer, str):
            buffer = buffer.encode("ascii")
        data = self.decode(buffer)
        if data is not None:
            return json.loads(data)

    def jwt_encode(self, payload):
        return JsonWebToken(self.alg, self.key, payload).encode()

    def jwt_decode(self, data, key=None):
        dec = self.jwt_decoder(data)
        dec.validate(key or self.key)
        return dec.payload

    def jwt_decoder(self, data):
        return JwtDecoder(self.allowed_algs, data)
